Write Modern Web Apps with the MEAN Stack: Mongo, Express, AngularJS, and Node.js by Jeff Dickey

Prerequisites:

1. MongoDB
2. Node.js

=======================================================================

How to run:

1. MongoDB
	* <MONGO_HOME>\bin\mongod --dbpath F:\<MONGO_HOME>\data\db
	* WIN example: F:\>MongoDB\bin\mongod --dbpath F:\MongoDB\data\db

2. Gulp/Node
	* cd <APP_HOME>
        * npm install
        * gulp dev